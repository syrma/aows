package com.aows.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.aows.types.Outil;

public class OutilDAOImpl implements OutilDAO {

	public static final String SQL_CONSULTER = "SELECT * FROM outil;";
	public static final String SQL_AJOUTER = "INSERT INTO outil(nom) VALUES(:nom)  ;";
	public static final String SQL_SUPPRIMER = "DELETE FROM outil WHERE id=:id;";
	public static final String SQL_SELECTATTRIBUTIONOUTIL="SELECT ido, idl FROM attributionoutil WHERE ido=:ido AND idl=:idl;";
	public static final String SQL_CONSULTERATTRIBUTIONOUTIL="SELECT ido, nombre FROM attributionoutil WHERE idl= :idl ;";
	public static final String SQL_ATTRIBUEROUTIL = "INSERT INTO attributionoutil values (:ido,:idl,:nombre);";
	public static final String SQL_MODIFIERATTRIBUEROUTIL = "UPDATE attributionoutil SET nombre= :nombre WHERE ido= :ido AND idl= :idl;";

	DAOFactory factory;
	public OutilDAOImpl(DAOFactory factory){
		this.factory = factory;
	}
	@Override
	public void attribuerOutil(int idl,int ido, int nbr)
	{
		//ajouter un eventul update
		Connection cnx = null;
		NamedParameterStatement stmt = null;
		try{
			cnx = factory.getConnection();
			stmt = new NamedParameterStatement(cnx, SQL_SELECTATTRIBUTIONOUTIL);
			stmt.setInt("ido", ido);
			stmt.setInt("idl", idl);
			ResultSet rs = stmt.executeQuery();
			if (rs.next())
			{
				cnx = factory.getConnection();
				stmt = new NamedParameterStatement(cnx, SQL_MODIFIERATTRIBUEROUTIL);
				stmt.setInt("ido", ido);
				stmt.setInt("idl", idl);
				stmt.setInt("nombre", nbr);

				int i = stmt.executeUpdate();
				if(i == 0)
					throw new DAOException("Impossible d'attribuer cet outil à ce local.");
			}
			else
			{
				cnx = factory.getConnection();
				stmt = new NamedParameterStatement(cnx, SQL_ATTRIBUEROUTIL);
				stmt.setInt("ido", ido);
				stmt.setInt("idl", idl);
				stmt.setInt("nombre", nbr);

				int i = stmt.executeUpdate();
				if(i == 0)
					throw new DAOException("Impossible d'attribuer cet outil à ce local.");
			}			
		}catch(SQLException ex){
			throw new DAOException(ex);
		}finally{
			try{
				stmt.close();
				cnx.close();			
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
		
		
		
		
	}
	@Override
	public Outil[] liste() {
		Connection cnx = null; 
		Statement stmt = null;
		ResultSet rs = null;
		Outil outil;
		ArrayList<Outil> outils = new ArrayList<Outil>();
		try{
			cnx = factory.getConnection();
			stmt = cnx.createStatement();
			rs = stmt.executeQuery(SQL_CONSULTER);
			while(rs.next()){
				outil = new Outil();
				outil.setId(rs.getInt("id"));
				outil.setNom(rs.getString("nom"));
				outils.add(outil);
			}
			if(outils.isEmpty())
				throw new DAOException("Il n'existe aucun outil.");
			return outils.toArray(new Outil[0]);
		}catch(SQLException ex){
			throw new DAOException(ex);
		}
	}
	public Outil[] consulterattribution(int idl) {
		Connection cnx = null;
		NamedParameterStatement stmt = null;
		
		Outil outil;
		ArrayList<Outil> outils = new ArrayList<Outil>();
		try{
			cnx = factory.getConnection();
			stmt = new NamedParameterStatement(cnx, SQL_CONSULTERATTRIBUTIONOUTIL);
		
			stmt.setInt("idl", idl);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				outil = new Outil();
				outil.setId(rs.getInt("ido"));
				outil.setNombre(rs.getInt("nombre"));
				outils.add(outil);
			}
			if(outils.isEmpty())
				{ Outil o = new Outil();
				o.setNombre(0);
				o.setId(0); 
				outils.add(o);}
			return outils.toArray(new Outil[0]);
		}catch(SQLException ex){
			throw new DAOException(ex);
		}
	}
	@Override
	public void ajouter(String nom) {
		Connection cnx = null;
		NamedParameterStatement stmt = null;
		try{
			cnx = factory.getConnection();
			stmt = new NamedParameterStatement(cnx, SQL_AJOUTER);
			stmt.setString("nom", nom);
			System.out.println("SQL à exécuter : \n"+ stmt);
			int i = stmt.executeUpdate();
			if(i == 0)
				throw new DAOException("Impossible de créer cet outil.");
		}catch(SQLException ex){
			throw new DAOException(ex);
		}finally{
			try{
				stmt.close();
				cnx.close();			
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void supprimer(int id) {
		Connection cnx = null;
		NamedParameterStatement stmt = null;
		try{
			cnx = factory.getConnection();
			stmt = new NamedParameterStatement(cnx, SQL_SUPPRIMER);
			stmt.setInt("id", id);
			System.out.println("SQL à exécuter : \n"+ stmt);
			int i = stmt.executeUpdate();
			if(i == 0)
				throw new DAOException("Impossible de supprimer cet outil.");
		}catch(SQLException ex){
			throw new DAOException(ex);
		}finally{
			try{
				stmt.close();
				cnx.close();
			}catch(SQLException e){
				e.printStackTrace();
			}

		}

	}
}