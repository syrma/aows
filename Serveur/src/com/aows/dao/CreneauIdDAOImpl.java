package com.aows.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.ArrayList;

import com.aows.types.Creneaux;

public class CreneauIdDAOImpl implements CreneauIdDAO {
	private final String SQL_CRENEAUX = "SELECT * FROM creneauid;";
	DAOFactory factory;

	public CreneauIdDAOImpl(){
		this.factory = DAOFactory.getInstance();	
	}
	
	@Override
	public Creneaux[] creneaux() {
		Connection cnx = null;
		Statement stmt = null;
		ResultSet rs = null;
		Creneaux c;
		ArrayList<Creneaux> cren = new ArrayList<Creneaux>();
        try{
			cnx = factory.getConnection();
			stmt = cnx.createStatement();
			rs = stmt.executeQuery(SQL_CRENEAUX);
			while(rs.next()){
			    c = new Creneaux();
				c.setIdcr(rs.getInt("idc"));
				c.setDcreneau(rs.getTime("dcreneau").toLocalTime());
				c.setFcreneau(rs.getTime("fcreneau").toLocalTime());
				cren.add(c);
			}
			if(cren.size() == 0)
				throw new DAOException("Aucun créneau.");			
		}catch(SQLException ex){
			throw new DAOException(ex);
		}finally{
			try{
       			rs.close();
		    	stmt.close();
			    cnx.close();
			}catch(SQLException ex){
				throw new DAOException(ex);
			}
		}
		return cren.toArray(new Creneaux[0]);
	}

}
