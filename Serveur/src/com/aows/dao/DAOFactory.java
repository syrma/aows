package com.aows.dao;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DAOFactory {
	private static final String FICHIER_PROPERTIES = "/com/aows/dao/dao.properties";
	private static final String PROPERTY_URL = "url";
	private static final String PROPERTY_DRIVER = "driver";
	private static final String PROPERTY_USERNAME = "username";
	private static final String PROPERTY_PASSWORD = "password";

	private String url;
	private String username;
	private String password;

	DAOFactory(String url, String username, String password) {
		this.url = url;
		this.username = username;
		this.password = password;
	}

	public static DAOFactory getInstance(){
		String url, driver, username, password; 
		Properties properties = new Properties();

		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		InputStream fichierp = loader.getResourceAsStream(FICHIER_PROPERTIES);
		if(fichierp==null)
			throw new DAOConfigurationException("Le fichier " + FICHIER_PROPERTIES + " est introuvable.");

		try{
			properties.load(fichierp);
			url = properties.getProperty(PROPERTY_URL);
			driver = properties.getProperty(PROPERTY_DRIVER);
			username = properties.getProperty(PROPERTY_USERNAME);
			password = properties.getProperty(PROPERTY_PASSWORD);
		}catch(IOException e){
			throw new DAOConfigurationException("Impossible de charger le fichier de configuration.",e);
		}

		try{
			Class.forName(driver);
		}catch(ClassNotFoundException e){
			throw new DAOConfigurationException("Impossible de charger le driver.",e);    		
		}

		return new DAOFactory(url,username,password);
	}
	public Connection getConnection() throws SQLException{
		return DriverManager.getConnection(url,username,password);
	}
	
	public CreneauDAO getCreneauDAO(){
		return new CreneauDAOImpl();
	}
	public CreneauIdDAO getCreneauIdDAO(){
		return new CreneauIdDAOImpl();
	}
	public LocalDAO getLocalDAO(){
		return new LocalDAOImpl();
	}
	
	public UtilisateurDAO getUtilisateurDAO(){
		return new UtilisateurDAOImpl();
	}
	
	public OutilDAO getOutilDAO(){
		return new OutilDAOImpl(this);
	}

}
