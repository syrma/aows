package com.aows.dao;

import com.aows.types.Utilisateur;

public interface UtilisateurDAO {
// souad
	
	Utilisateur [] consulter(String email);
	boolean authentification (String email, String mdp);
}
