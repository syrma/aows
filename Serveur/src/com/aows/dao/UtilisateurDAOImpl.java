package com.aows.dao;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.aows.types.Creneau;
import com.aows.types.Salle;
import com.aows.types.Utilisateur;
public class UtilisateurDAOImpl implements UtilisateurDAO {
	private static final String SQL_CONSULTERUTIL = "SELECT utilisateur.email, utilisateur.nom, utilisateur.prenom FROM utilisateur;";
	DAOFactory factory;
	private static final String SQL_AJOUTERUTIL = "INSERT INTO utilisateur values (:nom,:prenom,:mdp,:email); ";

	private static final String SQL_SUPPRIMERUTIL = "DELETE FROM local WHERE (email=:email); ";
	private static final String SQL_AUTHENTIFIER = "SELECT mdp FROM utilisateur WHERE (email=:email); ";

	
	

	public UtilisateurDAOImpl() {
		this.factory = DAOFactory.getInstance();
	}

	@Override
	public boolean authentification (String email, String mdp) 
		{
		
		Connection cnx = null;
		NamedParameterStatement stmt = null;
		ResultSet rs = null;
		String mdp1="";
		
        try{
			cnx = factory.getConnection();
			stmt = new NamedParameterStatement(cnx, SQL_AUTHENTIFIER);
			stmt.setString("email", email);
			rs = stmt.executeQuery();
			while(rs.next()){
			 
			mdp1=rs.getString("mdp");
			
				
			}
			if(mdp1=="")throw new DAOException("Utilisateur introuvable.");	
			if(!mdp.equals(mdp1)) 
			
				throw new DAOException("Mot de passe incorrect.");			
		}catch(SQLException ex){
			ex.printStackTrace();
			throw new DAOException(ex);
		}
		return true;
		}


	@Override
	public  Utilisateur [] consulter(String email) {
		Connection cnx = null;
		Statement stmt = null;
		ResultSet rs = null;
		Utilisateur utilisateur;
		ArrayList<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();
		try{
			cnx = factory.getConnection();
			stmt = cnx.createStatement();
			rs = stmt.executeQuery(SQL_CONSULTERUTIL);
			while(rs.next()){
				utilisateur = new Utilisateur();
				utilisateur.setNom(rs.getString("nom"));
				utilisateur.setPrenom(rs.getString("prenom"));
				utilisateur.setEmail(rs.getString("email"));
				
				utilisateurs.add(utilisateur);
			}
			if(utilisateurs.size() == 0)
				throw new DAOException("Aucun utilisateur trouvé.");			
		}catch(SQLException ex){
			throw new DAOException(ex);
		}
		return utilisateurs.toArray(new Utilisateur[0]);
		
	}
	

}
