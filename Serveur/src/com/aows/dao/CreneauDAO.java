package com.aows.dao;
import com.aows.types.Creneau;
import com.aows.types.Creneaux;
public interface CreneauDAO {

	public void reserver(String jour, int salle, int idcr, String mail);
	public Creneau[] liste(String email);
	public void annuler(int idc);
	
}
