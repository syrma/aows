package com.aows.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.aows.types.Creneau;
import com.aows.types.Creneaux;

public class CreneauDAOImpl implements CreneauDAO{
	private final String SQL_CONSULTER = "SELECT creneau.idc, nom, jour, dcreneau, fcreneau, email FROM Creneau JOIN Local ON id = idlocal JOIN creneauid ON creneauid.idc = idcr WHERE email = :email;";


	private final String SQL_RESERVER = "INSERT INTO creneau(idlocal, idcr, email, jour) SELECT :id, :idcr, :email, cast(:jour AS joursemaine)"
			+ "WHERE NOT chevauchement(:id, :idcr, :email, :jour);";


	private final String SQL_ANNULER = "DELETE FROM creneau WHERE :idc = idc; ";
	DAOFactory factory;


	public CreneauDAOImpl(){
		this.factory = DAOFactory.getInstance();	
	}
	@Override
	public void reserver(String jour, int salle, int idcr, String mail) {
		Connection cnx = null;
		NamedParameterStatement stmt = null;
		try{
			cnx = factory.getConnection();
			stmt = new NamedParameterStatement(cnx, SQL_RESERVER);
			stmt.setInt("id", salle);
			stmt.setInt("idcr",idcr);
			stmt.setString("email",mail);
			stmt.setString("jour",jour);
			System.out.println("SQL à exécuter : \n"+ stmt);
			int i = stmt.executeUpdate();
			if(i == 0)
				throw new DAOException("Impossible de réserver une salle avec ce créneau. Vous êtes probablement déjà pris dans une autre salle.");
		}catch(SQLException ex){
			ex.printStackTrace();
			throw new DAOException(ex);
		}finally{
			try{
				stmt.close();
				cnx.close();
			}catch(SQLException e){
				e.printStackTrace();
			}
		}

	}

	@Override
	public void annuler(int idc) {
		Connection cnx = null;
		NamedParameterStatement stmt = null;
		try{
			cnx = factory.getConnection();
			stmt = new NamedParameterStatement(cnx, SQL_ANNULER);
			stmt.setInt("idc", idc);
			System.out.println("SQL à exécuter : \n"+ stmt);
			int i = stmt.executeUpdate();
			if(i == 0)
				throw new DAOException("Impossible d'annuler cette réservation.");
		}catch(SQLException ex){
			throw new DAOException(ex);
		}finally{
			try{
				stmt.close();
				cnx.close();
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
	}
	@Override
	public Creneau[] liste(String email) {
		Connection cnx = null;
		NamedParameterStatement stmt = null;
		ResultSet rs = null;
		Creneau c;
		ArrayList<Creneau> reservations = new ArrayList<Creneau>();
        try{
			cnx = factory.getConnection();
			stmt = new NamedParameterStatement(cnx, SQL_CONSULTER);
			stmt.setString("email", email);
			rs = stmt.executeQuery();
			while(rs.next()){
			    c = new Creneau();
				c.setNomlocal(rs.getString("nom"));
				c.setDcreneau(rs.getTime("dcreneau").toLocalTime());
				c.setFcreneau(rs.getTime("fcreneau").toLocalTime());
				c.setEmail(rs.getString("email"));
				c.setJour(rs.getString("jour"));
				c.setIdc(rs.getInt("idc"));
				reservations.add(c);
			}
			if(reservations.size() == 0)
				throw new DAOException("Aucune réservation.");			
		}catch(SQLException ex){
			throw new DAOException(ex);
		}finally{
			try{
       			rs.close();
		    	stmt.close();
			    cnx.close();
			}catch(SQLException ex){
				throw new DAOException(ex);
			}
		}
		return reservations.toArray(new Creneau[0]);
	}
	
}