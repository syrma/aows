package com.aows.dao;

import java.time.LocalTime;

import com.aows.types.Salle;

public interface LocalDAO {
	//c fait
	Salle[] consulterLibres(int idcr, String jour);
	Salle[] consulter();
}
