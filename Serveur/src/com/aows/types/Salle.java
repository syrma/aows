package com.aows.types;

import java.io.Serializable;
import java.util.ArrayList;

public class Salle implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private  ArrayList<SalleOutil> outils;
	public ArrayList<SalleOutil> getOutils() {
		return outils;
	}


	public void setOutils(ArrayList<SalleOutil> outils) {
		this.outils = outils;
	}

	private int id;
	private String nomSalle;
	
	
	public Salle(){
		outils= new ArrayList<SalleOutil>();
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNomSalle() {
		return nomSalle;
	}
	public void setNomSalle(String nomSalle) {
		this.nomSalle = nomSalle;
	}
	
	@Override
	public String toString() {
		return "Salle [nomSalle=" + nomSalle + ", nbrpl=" +  ", pc="
				+ "]";
	}
	
	public void addOutil(String nom, Integer nombre){
		SalleOutil s = new SalleOutil(nom,nombre);
		outils.add(s);
	}
	
	public String[] nomsOutils(){
		String [] noms = new String [outils.size()];
		for(int i=0;i<outils.size();i++)
		{
			noms[i]=outils.get(i).getNom();
		}
		return noms;
	}
	
	public Integer nombreOutil(String nom){
		for(int i=0;i<outils.size();i++)
		{
			if(outils.get(i).getNom().equals(nom)){return outils.get(i).getNombre();}
		}
		return 0;
	}
	
}
