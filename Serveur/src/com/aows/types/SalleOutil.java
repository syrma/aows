package com.aows.types;

public class SalleOutil {
 private String nom;
 private int nombre ;
 
public SalleOutil(String nom, int nombre) {
	super();
	this.setNom(nom);
	this.setNombre(nombre);
}

public String getNom() {
	return nom;
}

public void setNom(String nom) {
	this.nom = nom;
}

public int getNombre() {
	return nombre;
}

public void setNombre(int nombre) {
	this.nombre = nombre;
}

@Override
public String toString() {
	return "SalleOutil [nom=" + nom + ", nombre=" + nombre + "]";
}
}
