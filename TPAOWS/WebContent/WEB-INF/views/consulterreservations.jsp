<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<%@ page import="com.aows.types.Creneau"%>
<%@ page import="com.aows.types.Creneaux"%>

<body>
 <div class="container">
          

<% Creneau[] creneaux = (Creneau[]) request.getAttribute("creneaux"); %> 
<% Creneaux[] horaires = (Creneaux[]) request.getAttribute("horaires"); %> 

      <div class="col-md-9">
        <div class="panel panel-success panel-successs  ">
          <div class="panel-heading">
          <h3 class="panel-title">Reservations</h3>
          </div>
          <div class="panel-body panel-bodyy">   
                  
                    <table class="table table-hover" >
                        <thead>
                            <tr>
                              <th>Jour</th>
                               <% if(horaires== null) out.println("<th>"+request.getAttribute("nohoraires")+"</th>");
                             else{ %>
                                <% for(Creneaux horaire : horaires){ %>
                             
                              <th><%=horaire.getDcreneau()%> - <%=horaire.getFcreneau() %></th>
                              
                             <%} %>
                              
                                
                              </tr>
                          </thead>
                          <tbody>
                            <% if(creneaux== null) out.println("<tr><td>"+request.getAttribute("nocreneaux")+"</td></tr>");
                             else{ %>
                          <tr>
                          <td>Samedi</td>
                          <% for(Creneaux   horaire : horaires){ %><td>
                           <% for(Creneau creneau : creneaux){
                        	   if(creneau.getJour().equals("Samedi")){
                        		 
                        	   if(creneau.getDcreneau().equals(horaire.getDcreneau())&&creneau.getFcreneau().equals(horaire.getFcreneau())){ %>
                               
                              <%=creneau.getNomlocal() %>
                               
                              
                         	   <% }} }%>
                           </td><%}%>
                          
                         
                          </tr>
                          <tr>
                          <td>Dimanche</td>
                          <% for(Creneaux   horaire : horaires){ %><td>
                           <% for(Creneau creneau : creneaux){
                        	   if(creneau.getJour().equals("Dimanche")){
                        		 
                        	   if(creneau.getDcreneau().equals(horaire.getDcreneau())&&creneau.getFcreneau().equals(horaire.getFcreneau())){ %>
                               
                              <%=creneau.getNomlocal() %>
                               
                              
                         	   <% }} }%>
                           </td><%}%>
                          </tr><tr>
                          <td>Lundi</td>
                          <% for(Creneaux   horaire : horaires){ %><td>
                           <% for(Creneau creneau : creneaux){
                        	   if(creneau.getJour().equals("Lundi")){
                        		 
                        	   if(creneau.getDcreneau().equals(horaire.getDcreneau())&&creneau.getFcreneau().equals(horaire.getFcreneau())){ %>
                               
                              <%=creneau.getNomlocal() %>
                               
                              
                         	   <% }} }%>
                           </td><%}%>
                          </tr><tr>
                          <td>Mardi</td>
                           <% for(Creneaux   horaire : horaires){ %><td>
                           <% for(Creneau creneau : creneaux){
                        	   if(creneau.getJour().equals("Mardi")){
                        		 
                        	   if(creneau.getDcreneau().equals(horaire.getDcreneau())&&creneau.getFcreneau().equals(horaire.getFcreneau())){ %>
                               
                              <%=creneau.getNomlocal() %>
                               
                              
                         	   <% }} }%>
                           </td><%}%>
                          </tr><tr>
                          <td>Mercredi</td>
                            <% for(Creneaux   horaire : horaires){ %><td>
                           <% for(Creneau creneau : creneaux){
                        	   if(creneau.getJour().equals("Mercredi")){
                        		 
                        	   if(creneau.getDcreneau().equals(horaire.getDcreneau())&&creneau.getFcreneau().equals(horaire.getFcreneau())){ %>
                               
                              <%=creneau.getNomlocal() %>
                               
                              
                         	   <% }} }%>
                           </td><%}%>
                          </tr><tr>
                          <td>Jeudi</td>
                          <% for(Creneaux   horaire : horaires){ %><td>
                           <% for(Creneau creneau : creneaux){
                        	   if(creneau.getJour().equals("Jeudi")){
                        		 
                        	   if(creneau.getDcreneau().equals(horaire.getDcreneau())&&creneau.getFcreneau().equals(horaire.getFcreneau())){ %>
                               
                              <%=creneau.getNomlocal() %>
                               
                              
                         	   <% }} }%>
                           </td><%}}}%>
                          </tr>
                            
                          </tbody>
                      </table>    
             
          </div>
          <!-- panel-body/ -->    

      </div>
      <!-- success/ -->
      </div>
      <!-- 9/ -->
  </div> 
  <!-- container/ -->


</body>
</html>