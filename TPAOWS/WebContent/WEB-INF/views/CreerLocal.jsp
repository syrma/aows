<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@include file="header.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ page import="com.aows.types.Salle"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Locaux</title>
</head>



<body>
<% Salle[] salles = (Salle[]) request.getAttribute("salles"); %>
Outils existants ; veuillez vérifier que votre outil ne se trouve pas dans cette liste avant de l'ajouter.
  <p>
   <select id= "outil" name = "outil">
   <%for(Salle salle : salles){ %>
   <option value = <%=salle.getId() %>> <%=salle.getNomSalle() %> </option>
   <%} %>
   </select>
 </p>
Entrez le nom de la salle à ajouter : 
<form method="post" action= "Ajouteroutil" >
<input type="text"  id = "nom" name = "nom" />
 <button type="submit"> Ajouter </button>
</form>
</body>

</html>