<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<%@ page import="com.aows.types.Creneaux"%>
  <div class="container">
          <div>
            <h3 class="page-header">Gestion des creneaux :</h3>
          </div>
            <div class="panel-body">
                <div class="row">
                    <form action="AjouterCreneau" method="post" accept-charset="utf-8">
                        <div class="col-md-4">
                           
                    <div class="input-group">
                      <span class="input-group-addon">Début</span>
                     
               <input type="number" class="form-control" id="hdcreneau" name="hdcreneau" placeholder="HH">
               <input type="number" class="form-control" id="mdcreneau" name="mdcreneau" placeholder="MM">
		              </div><br>
                    <div class="input-group">
                      <span class="input-group-addon">Fin</span>
               <input type="number" class="form-control" id="hfcreneau" name="hfcreneau" placeholder="HH">
               <input type="number" class="form-control" id="mfcreneau" name="mfcreneau" placeholder="MM">
		              </div></div>
                        </div>
                        <div class="col-md-4 ">
                       <button type="submit" class="btn btn-success col-md-6 col-md-offset-3 btn-flat">Ajouter creneau</button>                        </div>
                    </form>
                </div>
            </div>

<% Creneaux[] creneaux = (Creneaux[]) request.getAttribute("creneaux"); %> 
      <div class="col-md-9">
        <div class="panel panel-success panel-successs  ">
          <div class="panel-heading">
          <h3 class="panel-title">Gestion des creneaux</h3>
          </div>
          <div class="panel-body panel-bodyy">   
                  
                    <table class="table table-hover" >
                        <thead>
                            <tr>
                               <th>début</th>
                                <th>fin</th>
                              <th>Supprimer</th>
                              </tr>
                          </thead>
                          <tbody>
                             <% if(creneaux == null) out.println("<tr><td>"+request.getAttribute("notool")+"</td></tr>");
                             else{ %>
                         <% for(Creneaux creneau : creneaux){ %>
                              <tr>
                            
                                 <td><%=creneau.getDcreneau() %></td>
                                    <td><%=creneau.getFcreneau() %></td>
                              <td><form action="SupprimerCreneau" method = post><input name="creneau" type=hidden value ="<%=creneau.getIdcr() %>"> <button type="submit" class="btn btn-danger btn-flat btn-sm"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Supprimer</button></form></td>
                              </tr>
                             <%}} %>
                          </tbody>
                      </table>    
             
          </div>
          <!-- panel-body/ -->    

      </div>
      <!-- success/ -->
      </div>
      <!-- 9/ -->
  </div> 
  <!-- container/ -->

