<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="com.aows.types.Outil"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Supprimer outil</title>
</head>
<body>
<% Outil[] outils = (Outil[]) request.getAttribute("outils"); %>
   <form ACTION="Supprimeroutil" METHOD="post">
   <p>
   <select id= "outil" name = "outil">
   <%for(Outil outil : outils){ %>
   <option value = <%=outil.getId() %>> <%=outil.getNom() %> </option>
   <%} %>
   </select>
   </p>
   <button type="submit"> Supprimer </button>
   </p>
   </form>
</body>
</html>