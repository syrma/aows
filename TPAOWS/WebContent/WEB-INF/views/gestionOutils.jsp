<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<%@ page import="com.aows.types.Outil"%>
  <div class="container">
          <div>
            <h3 class="page-header">Gestion des outils :</h3>
          </div>
            <div class="panel-body">
                <div class="row">
                    <form action="Ajouteroutil" method="post" accept-charset="utf-8">
                        <div class="col-md-4">
                            <div class="input-group">
                                <span class="input-group-addon">Nom</span>
                                <input type="text" class="form-control" id="nom" name="nom" placeholder="Nom de l'outil">
                            </div>
                        </div>
                        <div class="col-md-3 ">
                       <button type="submit" class="btn btn-success col-md-6 col-md-offset-3 btn-flat">Ajouter outil</button>                        </div>
                    </form>
                </div>
            </div>

<% Outil[] outils = (Outil[]) request.getAttribute("outils"); %> 
      <div class="col-md-9">
        <div class="panel panel-success panel-successs  ">
          <div class="panel-heading">
          <h3 class="panel-title">Gestion des outils</h3>
          </div>
          <div class="panel-body panel-bodyy">   
                  
                    <table class="table table-hover" >
                        <thead>
                            <tr>
                              <th>Nom de l'outil</th>
                              <th>Supprimer</th>
                              </tr>
                          </thead>
                          <tbody>
                             <% if(outils == null) out.println("<tr><td>"+request.getAttribute("notool")+"</td></tr>");
                             else{ %>
                         <% for(Outil outil : outils){ %>
                              <tr>
                              <td><%=outil.getNom() %></td>
                              <td><form action="Supprimeroutil" method = post><input name="outil" type=hidden value ="<%=outil.getId() %>"> <button type="submit" class="btn btn-danger btn-flat btn-sm"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Supprimer</button></form></td>
                              </tr>
                             <%}} %>
                          </tbody>
                      </table>    
             
          </div>
          <!-- panel-body/ -->    

      </div>
      <!-- success/ -->
      </div>
      <!-- 9/ -->
  </div> 
  <!-- container/ -->

