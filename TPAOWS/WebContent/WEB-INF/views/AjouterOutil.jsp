<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ page import="com.aows.types.Outil"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Ajouter outil</title>
</head>
<body>
<% Outil[] outils = (Outil[]) request.getAttribute("outils"); %>
Outils existants ; veuillez vérifier que votre outil ne se trouve pas dans cette liste avant de l'ajouter.
  <p>
   <select id= "outil" name = "outil">
   <%for(Outil outil : outils){ %>
   <option value = <%=outil.getId() %>> <%=outil.getNom() %> </option>
   <%} %>
   </select>
 </p>
Entrez le nom de l'outil à ajouter : 
<form method="post" action= "Ajouteroutil" >
<input type="text"  id = "nom" name = "nom" />
 <button type="submit"> Ajouter </button>
</form>
</body>
</html>