<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<%@ page import="com.aows.types.Outil"%>
<body>
 <div class="container">
          <div>
            <h3 class="page-header">Attribution des outils :</h3>
          </div>
          
<% Outil[] outils = (Outil[]) request.getAttribute("outils"); %> 
<% Outil[] attributions = (Outil[]) request.getAttribute("attributions"); %> 

      <div class="col-md-9">
        <div class="panel panel-success panel-successs  ">
          <div class="panel-heading">
          <h3 class="panel-title">Attribution des outils</h3>
          </div>
          <div class="panel-body panel-bodyy">   
                   <form action="AttribuerOutil" method = post>
         <input name="idl" type=hidden value ="<%=request.getAttribute("idl") %>" > 
             
                    <table class="table table-hover" >
                        <thead>
                            <tr>
                              <th>Nom de l'outil</th>
                              <th>nombre</th>
              
                              </tr>
                          </thead>
                          <tbody>
                             <% if(outils == null) out.println("<tr><td>"+request.getAttribute("notool")+"</td></tr>");
                             else{ int i=0;%>
                            
          <% for(Outil outil : outils){ i++;%>
                              <tr>
                              <td> <input name="ido<%out.print(i); %>" type=hidden value ="<%=outil.getId()%>" > <%=outil.getNom() %></td>
                             
                            <!--   if(attributions[i-1].getId()==outil.getId()) -->
                              <td>  <input type="number" class="form-control" id="nbr<%out.print(i); %>" name="nbr<%out.print(i); %>" value="<%out.print(outil.getNombre()); %>"></td>
                             <input name="nbra<%out.print(i); %>" type=hidden value ="<%out.print(outil.getNombre()); %>" > 
								
                              </tr>
                              
                        
                             <%} } %>
                           
                          </tbody>
                      </table>   
                      
                         <button type="submit" class="btn btn-success col-md-6 col-md-offset-3 btn-flat">OK</button>    
       </form>
             
          </div>
          <!-- panel-body/ -->    

      </div>
      <!-- success/ -->
                            
      </div>
      <!-- 9/ -->
  </div> 
  <!-- container/ -->


</body>
</html>