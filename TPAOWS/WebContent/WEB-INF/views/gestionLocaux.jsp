<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<%@ page import="com.aows.types.Salle"%>
<body>
 <div class="container">
          <div>
            <h3 class="page-header">Gestion des locaux :</h3>
          </div>
            <div class="panel-body">
                <div class="row">
                    <form action="CreerLocal" method="post" accept-charset="utf-8">
                        <div class="col-md-4">
                            <div class="input-group">
                                <span class="input-group-addon">Nom</span>
                                <input type="text" class="form-control" id="nom" name="nom" placeholder="Nom du local">
                            </div>
                        </div>
                        <div class="col-md-3 ">
                       <button type="submit" class="btn btn-success col-md-6 col-md-offset-3 btn-flat">Ajouter local</button>                        </div>
                    </form>
                </div>
            </div>

<% Salle[] salles = (Salle[]) request.getAttribute("salles"); %> 
      <div class="col-md-9">
        <div class="panel panel-success panel-successs  ">
          <div class="panel-heading">
          <h3 class="panel-title">Gestion des salles</h3>
          </div>
          <div class="panel-body panel-bodyy">   
                  
                    <table class="table table-hover" >
                        <thead>
                            <tr>
                              <th>Nom de la salle</th>
                              <th>Supprimer</th>
                               <th>Attribuer outil</th>
                              </tr>
                          </thead>
                          <tbody>
                             <% if(salles == null) out.println("<tr><td>"+request.getAttribute("nosalle")+"</td></tr>");
                             else{ %>
                         <% for(Salle salle : salles){ %>
                              <tr>
                              <td><%=salle.getNomSalle() %></td>
                              <td><form action="SupprimerLocal" method = post><input name="id" type=hidden value ="<%=salle.getId() %>"> <button type="submit" class="btn btn-danger btn-flat btn-sm"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Supprimer</button></form></td>
                              <td><form action="AttribuerOutil" method = get><input name="idl" type=hidden value ="<%=salle.getId() %>"> <button type="submit" class="btn btn-primary btn-flat btn-sm"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> outils</button></form></td>

                              </tr>
                             <%}} %>
                          </tbody>
                      </table>    
             
          </div>
          <!-- panel-body/ -->    

      </div>
      <!-- success/ -->
      </div>
      <!-- 9/ -->
  </div> 
  <!-- container/ -->


</body>
</html>