<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<%@ page import="com.aows.types.Utilisateur"%>
<body>
 <div class="container">
          <div>
            <h3 class="page-header">Gestion des Utilisateurs :</h3>
          </div>
           

<% Utilisateur[] utilisateurs = (Utilisateur[]) request.getAttribute("utilisateurs"); %> 
      <div class="col-md-9">
        <div class="panel panel-success panel-successs  ">
          <div class="panel-heading">
          <h3 class="panel-title">Gestion des utilisateurs</h3>
          </div>
          <div class="panel-body panel-bodyy">   
                  
                    <table class="table table-hover" >
                        <thead>
                            <tr>
                              <th>Nom </th>
                              <th>Prénom</th>
                                <th>email</th>
                               <th>Supprimer</th>
                              </tr>
                          </thead>
                          <tbody>
                          
                             <% if(utilisateurs == null) out.println("<tr><td>"+request.getAttribute("aucun")+"</td></tr>");
                             else{ %>
                         <% for(Utilisateur utilisateur : utilisateurs){ %>
                              <tr>
                              <td><%=utilisateur.getNom() %></td>
                              <td><%=utilisateur.getPrenom() %></td>
                              <td><%=utilisateur.getEmail() %></td>
                              <td><form action="SupprimerUtilisateur" method = post><input name="id" type=hidden value ="<%=utilisateur.getEmail() %>"> <button type="submit" class="btn btn-danger btn-flat btn-sm"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Supprimer</button></form></td>

                              </tr>
                             <%}} %>
                                <% if(request.getAttribute("impossible") != null) out.println("<tr><td>"+request.getAttribute("impossible")+"</td></tr>");
                             %>
                          </tbody>
                      </table>    
             
          </div>
          <!-- panel-body/ -->    

      </div>
      <!-- success/ -->
      </div>
      <!-- 9/ -->
  </div> 
  <!-- container/ -->


</body>
</html>