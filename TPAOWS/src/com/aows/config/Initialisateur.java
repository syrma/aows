package com.aows.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.IOException;
import java.util.ArrayList;

import com.aows.dao.DAOFactory;


public class Initialisateur implements ServletContextListener {

	private DAOFactory factory ;
	public static final String ATTRIBUT_FACTORY = "daofactory"; 

	@Override
	public void contextInitialized(ServletContextEvent event) {
		ServletContext context = event.getServletContext() ;		
		factory = DAOFactory.getInstance();		
		context.setAttribute(ATTRIBUT_FACTORY, factory);
	}
	
	
	@Override
	public void contextDestroyed(ServletContextEvent event) {
    }

}
