package com.aows.types;

import java.io.Serializable;
import java.util.HashMap;

public class Salle implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private HashMap<String, Integer> outils;
	private int id;
	private String nomSalle;
	private int nbrpl;
	private boolean pc;
	
	public Salle(){
		outils = new HashMap<String, Integer>();
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNomSalle() {
		return nomSalle;
	}
	public void setNomSalle(String nomSalle) {
		this.nomSalle = nomSalle;
	}
	public int getNbrpl() {
		return nbrpl;
	}
	public void setNbrpl(int nbrpl) {
		this.nbrpl = nbrpl;
	}
	public boolean hasPc() {
		return pc;
	}
	public void setPc(boolean pc) {
		this.pc = pc;
	}
	@Override
	public String toString() {
		return "Salle [nomSalle=" + nomSalle + ", nbrpl=" + nbrpl + ", pc="
				+ pc + "]";
	}
	
	public void addOutil(String nom, Integer nombre){
		outils.put(nom, nombre);
	}
	
	public String[] nomsOutils(){
		return outils.keySet().toArray(new String[0]);
	}
	
	public Integer nombreOutil(String nom){
		return outils.get(nom);
	}
	
}
