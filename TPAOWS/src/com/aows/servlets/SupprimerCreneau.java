package com.aows.servlets;

import static com.aows.config.Initialisateur.ATTRIBUT_FACTORY;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.aows.dao.CreneauIdDAO;
import com.aows.dao.DAOException;
import com.aows.dao.DAOFactory;
import com.aows.types.Creneaux;
import com.aows.types.Outil;

/**
 * Servlet implementation class SupprimerCreneau
 */
@WebServlet("/SupprimerCreneau")
public class SupprimerCreneau extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private static final String VUE = "/WEB-INF/views/gestionCreneaux.jsp";

	private CreneauIdDAO cidao;
    /**
     * @see HttpServlet#HttpServlet()
     */
	public void init(){
		DAOFactory df = (DAOFactory) getServletContext().getAttribute(ATTRIBUT_FACTORY);
		cidao = df.getCreneauIdDAO();
	}
    public SupprimerCreneau() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(true){ //TODO: remplacer true par le test d'authentification lorsqu'il sera fait
			try{
//				ArrayList<String> errors = new ArrayList<String>();
				Creneaux[] creneaux = cidao.creneaux();
				request.setAttribute("creneaux", creneaux);
			}catch(DAOException ex){
				request.setAttribute("notool", ex.getMessage());
			}finally{
				request.getRequestDispatcher(VUE).forward(request, response);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(true){ //TODO: remplacer true par le test d'authentification lorsqu'il sera fait
			try{
				int id = Integer.parseInt(request.getParameter("creneau"));
				cidao.supprimerCreneau(id);
				response.sendRedirect("SupprimerCreneau");
			}catch(NumberFormatException ex){
				response.sendError(500, "Une erreur est survenue.");
			}catch(DAOException ex){
				response.sendError(500, ex.getMessage());
			}
		}
	}

}
