package com.aows.servlets;

import static com.aows.config.Initialisateur.ATTRIBUT_FACTORY;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.aows.dao.DAOException;
import com.aows.dao.DAOFactory;
import com.aows.dao.UtilisateurDAO;
import com.aows.types.Utilisateur;

/**
 * Servlet implementation class GestionUtilisateurs
 */
@WebServlet("/GestionUtilisateurs")
public class GestionUtilisateurs extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String VUE = "/WEB-INF/views/Gestionutilisateurs.jsp";
	private UtilisateurDAO udao;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GestionUtilisateurs() {
        super();
        // TODO Auto-generated constructor stub
    }
    public void init(){
  		DAOFactory df = (DAOFactory) getServletContext().getAttribute(ATTRIBUT_FACTORY);
  		udao = df.getUtilisateurDAO();
  	}
     

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try{
			Utilisateur[] utilisateurs =udao.consulter();
		request.setAttribute("utilisateurs", utilisateurs);
	}catch(DAOException ex){
		request.setAttribute("aucun", ex.getMessage());
	}finally{
		request.getRequestDispatcher(VUE).forward(request, response);
	}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
