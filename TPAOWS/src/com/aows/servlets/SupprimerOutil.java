package com.aows.servlets;

import static com.aows.config.Initialisateur.ATTRIBUT_FACTORY;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.aows.dao.DAOException;
import com.aows.dao.DAOFactory;
import com.aows.dao.OutilDAO;
import com.aows.types.Outil;
/**
 * Servlet implementation class Test
 */
public class SupprimerOutil extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String VUE = "/WEB-INF/views/gestionOutils.jsp";

	private OutilDAO odao;

	/**	
	 * @see HttpServlet#HttpServlet()
	 */
	public void init(){
		DAOFactory df = (DAOFactory) getServletContext().getAttribute(ATTRIBUT_FACTORY);
		odao = df.getOutilDAO();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(true){ //TODO: remplacer true par le test d'authentification lorsqu'il sera fait
			try{
//				ArrayList<String> errors = new ArrayList<String>();
				Outil[] outils = odao.liste();
				request.setAttribute("outils", outils);
			}catch(DAOException ex){
				request.setAttribute("notool", ex.getMessage());
			}finally{
				request.getRequestDispatcher(VUE).forward(request, response);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(true){ //TODO: remplacer true par le test d'authentification lorsqu'il sera fait
			try{
				int id = Integer.parseInt(request.getParameter("outil"));
				odao.supprimer(id);
				response.sendRedirect("Supprimeroutil");
			}catch(NumberFormatException ex){
				response.sendError(500, "Une erreur est survenue.");
			}catch(DAOException ex){
				response.sendError(500, ex.getMessage());
			}
		}
	}

}