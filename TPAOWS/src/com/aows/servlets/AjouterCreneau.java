package com.aows.servlets;

import static com.aows.config.Initialisateur.ATTRIBUT_FACTORY;

import java.io.IOException;
import java.sql.Time;
import java.time.LocalTime;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.aows.dao.CreneauIdDAO;
import com.aows.dao.DAOException;
import com.aows.dao.DAOFactory;
import com.aows.types.Creneaux;

/**
 * Servlet implementation class AjouterCreneau
 */
@WebServlet("/AjouterCreneau")
public class AjouterCreneau extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String VUE = "/WEB-INF/views/gestionCreneaux.jsp";

	private CreneauIdDAO cidao;
    /**
     * @see HttpServlet#HttpServlet()
     */
	public void init(){
		DAOFactory df = (DAOFactory) getServletContext().getAttribute(ATTRIBUT_FACTORY);
		cidao = df.getCreneauIdDAO();
	}
    public AjouterCreneau() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(true){ //TODO: remplacer true par le test d'authentification lorsqu'il sera fait
			try{
//				ArrayList<String> errors = new ArrayList<String>();
				Creneaux[] creneaux = cidao.creneaux();
				request.setAttribute("creneaux", creneaux);
			}catch(DAOException ex){
				request.setAttribute("notool", ex.getMessage());
			}finally{
				request.getRequestDispatcher(VUE).forward(request, response);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(true){ //TODO: remplacer true par le test d'authentification lorsqu'il sera fait
			try{
				
				int hdcreneau = Integer.valueOf(request.getParameter("hdcreneau"));
				int mdcreneau = Integer.valueOf(request.getParameter("mdcreneau"));
				LocalTime dcreneau = LocalTime.of(hdcreneau, mdcreneau);
				
				int hfcreneau = Integer.valueOf(request.getParameter("hfcreneau"));
				int mfcreneau = Integer.valueOf(request.getParameter("mfcreneau"));
				LocalTime fcreneau = LocalTime.of(hfcreneau, mfcreneau);
				Creneaux c = new Creneaux();
			
				c.setDcreneau(dcreneau);
				c.setFcreneau(fcreneau); 
				cidao.ajouterCreneau(c);
				
				response.sendRedirect("AjouterCreneau");
			}catch(DAOException ex){
				response.sendError(500, ex.getMessage());
			}
		}
	}

}
