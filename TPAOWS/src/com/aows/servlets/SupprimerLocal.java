package com.aows.servlets;

import static com.aows.config.Initialisateur.ATTRIBUT_FACTORY;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.aows.dao.DAOException;
import com.aows.dao.DAOFactory;
import com.aows.dao.LocalDAO;
import com.aows.types.Salle;

/**
 * Servlet implementation class SupprimerLocal
 */
@WebServlet("/SupprimerLocal")
public class SupprimerLocal extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String VUE = "/WEB-INF/views/gestionLocaux.jsp";
	private LocalDAO ldao;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SupprimerLocal() {
        super();
        // TODO Auto-generated constructor stub
    }
    public void init(){
  		DAOFactory df = (DAOFactory) getServletContext().getAttribute(ATTRIBUT_FACTORY);
  		ldao = df.getLocalDAO();
  	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(true){ //TODO: remplacer true par le test d'authentification lorsqu'il sera fait
			try{
					Salle[] salles =ldao.consulter();
					//System.out.println("trerererere"+salles[0].toString());
				request.setAttribute("salles", salles);
			}catch(DAOException ex){
				request.setAttribute("nosalle", ex.getMessage());
			}finally{
				request.getRequestDispatcher(VUE).forward(request, response);
			}
		}

	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(true){ //TODO: remplacer true par le test d'authentification lorsqu'il sera fait
			try{
				System.out.println(request.getParameter("id")); 
		int idlocal = Integer.valueOf(request.getParameter("id"));
		ldao.supprimerLocal(idlocal); 
		response.sendRedirect("SupprimerLocal");
			}catch(NumberFormatException ex){
				response.sendError(500, "Une erreur est survenue.");
			}catch(DAOException ex){
				response.sendError(500, ex.getMessage());
			}
		
	}

}}
