package com.aows.servlets;

import static com.aows.config.Initialisateur.ATTRIBUT_FACTORY;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.aows.dao.CreneauDAO;
import com.aows.dao.CreneauIdDAO;
import com.aows.dao.DAOException;
import com.aows.dao.DAOFactory;
import com.aows.types.Creneau;
import com.aows.types.Creneaux;

/**
 * Servlet implementation class ConsulterReservations
 */
@WebServlet("/ConsulterReservations")
public class ConsulterReservations extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String VUE = "/WEB-INF/views/consulterreservations.jsp";
	private CreneauDAO cdao;
	private CreneauIdDAO ccdao;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConsulterReservations() {
        super();
        // TODO Auto-generated constructor stub
    }
    public void init(){
		DAOFactory df = (DAOFactory) getServletContext().getAttribute(ATTRIBUT_FACTORY);
		cdao = df.getCreneauDAO();
		ccdao = df.getCreneauIdDAO();

	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try{
			Creneaux [] cid = ccdao.creneaux();
			
			
			request.setAttribute("horaires", cid);
		
	

	}catch(DAOException ex){
		request.setAttribute("nohoraires", ex.getMessage());
	}
		try{
			
			Creneau[] creneaux =cdao.liste();
			//System.out.println("trerererere"+salles[0].toString());
		
		request.setAttribute("creneaux", creneaux);
	

	}catch(DAOException ex){
		request.setAttribute("nocreneaux", ex.getMessage());
	}finally{
		request.getRequestDispatcher(VUE).forward(request, response);
	}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
