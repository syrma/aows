package com.aows.servlets;

import static com.aows.config.Initialisateur.ATTRIBUT_FACTORY;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.aows.dao.DAOException;
import com.aows.dao.DAOFactory;
import com.aows.dao.OutilDAO;
import com.aows.types.Outil;

/**
 * Servlet implementation class AttribuerOutil
 */
@WebServlet("/AttribuerOutil")
public class AttribuerOutil extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String VUE = "/WEB-INF/views/AttribuerOutils.jsp";
	private OutilDAO odao;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AttribuerOutil() {
        super();
        // TODO Auto-generated constructor stub
    }
    public void init(){
		DAOFactory df = (DAOFactory) getServletContext().getAttribute(ATTRIBUT_FACTORY);
		odao = df.getOutilDAO();
	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(true){ //TODO: remplacer true par le test d'authentification lorsqu'il sera fait
			try{
				int idl = Integer.valueOf(request.getParameter("idl"));
				request.setAttribute("idl", idl);
				Outil [] attributions = odao.consulterattribution(idl);
				Outil[] outils = odao.liste();
				ArrayList<Outil> a = new ArrayList<Outil>();
				for(int i=0 ; i<outils.length;i++){
					Outil o = new Outil ();
					o.setId(outils[i].getId());
					o.setNom(outils[i].getNom()); 
					//o.setNombre(0); 
					for(int j=0;j<attributions.length;j++)
					{
						if(o.getId()==attributions[j].getId())
							o.setNombre(attributions[j].getNombre());  				
						
					}
					a.add(o);
				}
				Outil [] mesoutils = a.toArray(new Outil[0]);
				request.setAttribute("outils", mesoutils);
				
			}catch(DAOException ex){
				request.setAttribute("notool", ex.getMessage());
			}finally{
				request.getRequestDispatcher(VUE).forward(request, response);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int idl = Integer.valueOf(request.getParameter("idl"));
		
		int i=1;
		while(request.getParameter("ido"+i)!=null)
		{
		int ido = Integer.valueOf(request.getParameter("ido"+i));
		int nbr = Integer.valueOf(request.getParameter("nbr"+i));
		 //TODO:
		if(nbr!=0)odao.attribuerOutil( idl, ido,nbr);
		i++;
		}
		
		
		response.sendRedirect("CreerLocal");  
	}

}
