package com.aows.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.time.LocalTime;
import java.util.ArrayList;

import com.aows.types.Salle;

public class LocalDAOImpl implements LocalDAO {
    // SQL_CONSULTER ramène toutes les salles qui n'ont aucune réservation. À utiliser dans supprimer local.
	private static final String SQL_CONSULTER = "SELECT * FROM local LEFT JOIN creneau ON idlocal = id WHERE email IS NULL;";

	private static final String SQL_CONSULTER_PAR_CRENEAU = "SELECT local.id AS idloc, local.nom AS nomloc, outil.id AS idout, outil.nom AS nomout, nombre FROM local JOIN outil ON TRUE LEFT JOIN attributionoutil ON idl = local.id AND ido = outil.id WHERE NOT EXISTS (SELECT * FROM creneau WHERE idlocal = local.id AND idcr = :idcr AND jour = cast(:jour as joursemaine)) ORDER BY idloc;";

	private static final String SQL_CREERLOCAL = "INSERT INTO local (nom) values (:nom); ";

	private static final String SQL_SUPPRIMERLOCAL = "DELETE FROM local WHERE (id=:id); ";

	DAOFactory factory;
	
	LocalDAOImpl(DAOFactory factory){
		this.factory = factory;
	}
	@Override
	public Salle[] consulter() {
		Connection cnx = null;
		NamedParameterStatement stmt = null;
		ResultSet rs = null;
		Salle salle;
		ArrayList<Salle> salles = new ArrayList<Salle>();
		try{
			cnx = factory.getConnection();
			stmt = new NamedParameterStatement(cnx, SQL_CONSULTER);
			
			rs = stmt.executeQuery();
			while(rs.next()){
				salle = new Salle();
				salle.setId(rs.getInt("id"));
				salle.setNomSalle(rs.getString("nom"));
				salles.add(salle);
			}
			if(salles.isEmpty())
				
				{
				throw new DAOException("Il n'existe aucune salle libre.");	}
			
			
		}catch(SQLException ex){
			throw new DAOException("Il n'existe aucune salle libre.");	
			//throw new DAOException(ex);
		}
		return salles.toArray(new Salle[0]);
		
	}
	@Override
	public Salle[] consulterLibres(int idcr, String jour) {
		Connection cnx = null;
		NamedParameterStatement stmt = null;
		ResultSet rs = null;
		String nom;
		Integer nombre;
		Salle salle = new Salle();
		ArrayList<Salle> salles = new ArrayList<Salle>();
		try{
			cnx = factory.getConnection();
			stmt = new NamedParameterStatement(cnx, SQL_CONSULTER_PAR_CRENEAU);
			stmt.setInt("idcr", idcr);
			stmt.setString("jour", jour);
			System.out.println("SQL à exécuter : " + stmt);
			rs = stmt.executeQuery();
			boolean next;
			if(next = rs.next()){				
				nom = rs.getString("nomloc");
			do{
				salle = new Salle();
				salle.setId(rs.getInt("idloc"));
				salle.setNomSalle(nom);
				while(next && (nom = rs.getString("nomloc")).equals(salle.getNomSalle())){
					
					nombre = rs.getInt("nombre");
					if(nombre == null)
						nombre = 0;
					salle.addOutil(rs.getString("nomout"), nombre);
					next = rs.next();
			}
				salles.add(salle);
			}while(next);
			}
			if(salles.size() == 0)
				throw new DAOException("Aucune salle libre.");			
		}catch(SQLException ex){
			throw new DAOException(ex);
		}
		return salles.toArray(new Salle[0]);
		
	}

	@Override
	public void creerLocal( String nom) {
		Connection cnx = null;
		NamedParameterStatement stmt = null;
		try{
			cnx = factory.getConnection();
			stmt = new NamedParameterStatement(cnx, SQL_CREERLOCAL);
			stmt.setString("nom", nom);
			System.out.println("SQL à exécuter : \n"+ stmt);
			int i = stmt.executeUpdate();
			if(i == 0)
				throw new DAOException("Impossible de créer cette salle.");
		}catch(SQLException ex){
			throw new DAOException(ex);
		}finally{
			try{
				stmt.close();
				cnx.close();			
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void supprimerLocal(int id) {
		Connection cnx = null;
		NamedParameterStatement stmt = null;
		try{
			cnx = factory.getConnection();
			stmt = new NamedParameterStatement(cnx, SQL_SUPPRIMERLOCAL);
			stmt.setInt("id", id);
			
			System.out.println("SQL à exécuter : \n"+ stmt);
			int i = stmt.executeUpdate();
			if(i == 0)
				throw new DAOException("Impossible de supprimer cette salle.");
		}catch(SQLException ex){
			
			throw new DAOException("la salle n'est pas libre",ex); 
		}finally{
			try{
				stmt.close();
				cnx.close();
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
	}

}
