package com.aows.dao;

import com.aows.types.Utilisateur;

public interface UtilisateurDAO {

	void supprimer (String email);
	Utilisateur [] consulter();
}
