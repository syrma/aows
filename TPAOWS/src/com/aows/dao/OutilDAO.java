package com.aows.dao;

import com.aows.types.Outil;

public interface OutilDAO {
	Outil[] liste();
	void ajouter(String nom);
	void supprimer(int id);
	void attribuerOutil(int idl,int ido, int nbr);
	Outil[] consulterattribution(int idl);
}
