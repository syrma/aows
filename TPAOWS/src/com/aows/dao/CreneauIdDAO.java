package com.aows.dao;

import com.aows.types.Creneaux;

public interface CreneauIdDAO {
void ajouterCreneau(Creneaux c);
void supprimerCreneau (int idc);
Creneaux[] creneaux();
}
