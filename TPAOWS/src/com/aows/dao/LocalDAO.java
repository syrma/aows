package com.aows.dao;

import java.time.LocalTime;

import com.aows.types.Salle;

public interface LocalDAO {
	
	Salle[] consulterLibres(int idcr, String jour);
	Salle[] consulter();
	void creerLocal(String nom);
	void supprimerLocal(int id);

}
