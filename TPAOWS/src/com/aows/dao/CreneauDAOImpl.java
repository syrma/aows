package com.aows.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.aows.types.Creneau;
import com.aows.types.Creneaux;

public class CreneauDAOImpl implements CreneauDAO{
	private final String SQL_CONSULTER = "SELECT creneau.idc, nom, jour, dcreneau, fcreneau, email FROM Creneau JOIN Local ON id = idlocal JOIN creneauid ON creneauid.idc = idcr;";

	DAOFactory factory;

	CreneauDAOImpl(DAOFactory factory){
		this.factory = factory;	
	}


	
	@Override
	public Creneau[] liste() {
		Connection cnx = null;
		NamedParameterStatement stmt = null;
		ResultSet rs = null;
		Creneau c;
		ArrayList<Creneau> reservations = new ArrayList<Creneau>();
        try{
			cnx = factory.getConnection();
			stmt = new NamedParameterStatement(cnx, SQL_CONSULTER);
		
			rs = stmt.executeQuery();
			while(rs.next()){
			    c = new Creneau();
			    c.setEmail(rs.getString("email"));
				c.setNomlocal(rs.getString("nom"));
				c.setDcreneau(rs.getTime("dcreneau").toLocalTime());
				c.setFcreneau(rs.getTime("fcreneau").toLocalTime());
				c.setEmail(rs.getString("email"));
				c.setJour(rs.getString("jour"));
				c.setIdc(rs.getInt("idc"));
				reservations.add(c);
			}
			if(reservations.size() == 0)
				throw new DAOException("Aucune réservation.");			
		}catch(SQLException ex){
			throw new DAOException(ex);
		}finally{
			try{
       			rs.close();
		    	stmt.close();
			    cnx.close();
			}catch(SQLException ex){
				throw new DAOException(ex);
			}
		}
		return reservations.toArray(new Creneau[0]);
	}
	
}