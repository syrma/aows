package com.aows.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.ArrayList;

import com.aows.types.Creneaux;

public class CreneauIdDAOImpl implements CreneauIdDAO {
	private final String SQL_AJOUTERC = "INSERT INTO creneauid(dcreneau, fcreneau) SELECT :dcreneau, :fcreneau "+ "WHERE NOT chevauchementcreneau(:dcreneau, :fcreneau);";
	private final String SQL_SUPPRIMERC = "DELETE FROM creneauid WHERE idc=:idc;";
	private final String SQL_CRENEAUX = "SELECT * FROM creneauid;";
	DAOFactory factory;

	CreneauIdDAOImpl(DAOFactory factory){
		this.factory = factory;	
	}
	@Override
	public void ajouterCreneau(Creneaux c) {
		// TODO Auto-generated method stub
		Connection cnx = null;
		NamedParameterStatement stmt = null;
		try{
			cnx = factory.getConnection();
			stmt = new NamedParameterStatement(cnx, SQL_AJOUTERC);
			stmt.setTime("dcreneau",Time.valueOf(c.getDcreneau()));//new Time(c.getDcreneau().getSecond())); 
			stmt.setTime("fcreneau",Time.valueOf(c.getFcreneau()));//new Time(c.getFcreneau().getSecond())); 
			System.out.println("SQL à exécuter : \n"+ stmt);
			int i = stmt.executeUpdate();
			if(i == 0)
				throw new DAOException("Impossible d'ajouter ce creneau.");
		}catch(SQLException ex){
			throw new DAOException(ex);
		}finally{
			try{
				stmt.close();
				cnx.close();			
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
	}

	@Override
	public void supprimerCreneau(int idc) {
		// TODO Auto-generated method stub
		Connection cnx = null;
		NamedParameterStatement stmt = null;
		try{
			cnx = factory.getConnection();
			stmt = new NamedParameterStatement(cnx, SQL_SUPPRIMERC);
			stmt.setInt("idc", idc);
			System.out.println("SQL à exécuter : \n"+ stmt);
			int i = stmt.executeUpdate();
			if(i == 0)
				throw new DAOException("Impossible de supprimer ce creneau.");
		}catch(SQLException ex){
			throw new DAOException(ex);
		}finally{
			try{
				stmt.close();
				cnx.close();
			}catch(SQLException e){
				e.printStackTrace();
			}

		}

	}
	@Override
	public Creneaux[] creneaux() {
		Connection cnx = null;
		Statement stmt = null;
		ResultSet rs = null;
		Creneaux c;
		ArrayList<Creneaux> cren = new ArrayList<Creneaux>();
        try{
			cnx = factory.getConnection();
			stmt = cnx.createStatement();
			rs = stmt.executeQuery(SQL_CRENEAUX);
			while(rs.next()){
			    c = new Creneaux();
				c.setIdcr(rs.getInt("idc"));
				c.setDcreneau(rs.getTime("dcreneau").toLocalTime());
				c.setFcreneau(rs.getTime("fcreneau").toLocalTime());
				cren.add(c);
			}
			if(cren.size() == 0)
				throw new DAOException("Aucun créneau.");			
		}catch(SQLException ex){
			throw new DAOException(ex);
		}finally{
			try{
       			rs.close();
		    	stmt.close();
			    cnx.close();
			}catch(SQLException ex){
				throw new DAOException(ex);
			}
		}
		return cren.toArray(new Creneaux[0]);
	}

}
