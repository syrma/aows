package com.aows.dao;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.aows.types.Creneau;
import com.aows.types.Salle;
import com.aows.types.Utilisateur;
public class UtilisateurDAOImpl implements UtilisateurDAO {
	private static final String SQL_CONSULTERUTIL = "SELECT utilisateur.email, utilisateur.nom, utilisateur.prenom FROM utilisateur;";
	DAOFactory factory;
	private static final String SQL_AJOUTERUTIL = "INSERT INTO utilisateur values (:nom,:prenom,:mdp,:email); ";

	private static final String SQL_SUPPRIMERUTIL = "DELETE FROM utilisateur WHERE (email=:email); ";
	private static final String SQL_AUTHENTIFIER = "SELECT mdp FROM utilisateur WHERE (email=:email); ";

	
	

	public UtilisateurDAOImpl(DAOFactory daoFactory) {
		factory = daoFactory;
	}


	

	@Override
	public  Utilisateur [] consulter() {
		Connection cnx = null;
		Statement stmt = null;
		ResultSet rs = null;
		Utilisateur utilisateur;
		ArrayList<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();
		try{
			cnx = factory.getConnection();
			stmt = cnx.createStatement();
			rs = stmt.executeQuery(SQL_CONSULTERUTIL);
			while(rs.next()){
				utilisateur = new Utilisateur();
				utilisateur.setNom(rs.getString("nom"));
				utilisateur.setPrenom(rs.getString("prenom"));
				utilisateur.setEmail(rs.getString("email"));
				
				utilisateurs.add(utilisateur);
			}
			if(utilisateurs.size() == 0)
				throw new DAOException("Aucun utilisateur trouvé.");			
		}catch(SQLException ex){
			throw new DAOException(ex);
		}
		return utilisateurs.toArray(new Utilisateur[0]);
		
	}
	@Override
	public void supprimer(String email) {
		Connection cnx = null;
		NamedParameterStatement stmt = null;
		try{
			cnx = factory.getConnection();
			stmt = new NamedParameterStatement(cnx, SQL_SUPPRIMERUTIL);
			stmt.setString("email", email);
			System.out.println("SQL à exécuter : \n"+ stmt);
			int i = stmt.executeUpdate();
			if(i == 0)
				throw new DAOException("Impossible de supprimer cet utilisateur.");
		}catch(SQLException ex){
			throw new DAOException(ex);
		}finally{
			try{
				stmt.close();
				cnx.close();
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
	}

}
