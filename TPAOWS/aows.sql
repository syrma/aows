﻿DROP TABLE IF EXISTS attributionoutil, creneau, outil, local, utilisateur, creneauid;
CREATE TYPE joursemaine AS ENUM('Samedi', 'Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi');


CREATE TABLE local(
       id SERIAL NOT NULL,
       nom varchar NOT NULL,
       PRIMARY KEY(id)
       );

CREATE TABLE outil(
       id SERIAL NOT NULL,
       nom VARCHAR NOT NULL UNIQUE,
       PRIMARY KEY(id)
       );

CREATE TABLE attributionoutil(
       ido INT NOT NULL,
       idl INT NOT NULL,
       nombre INT NOT NULL,
       PRIMARY KEY(ido, idl),
       CONSTRAINT salle1 FOREIGN KEY(idl) REFERENCES local(id),
       CONSTRAINT outil FOREIGN KEY(ido) REFERENCES outil(id)       
       );
       
CREATE TABLE utilisateur(
       nom VARCHAR(50) NOT NULL,
       prenom VARCHAR(50) NOT NULL,
       mdp VARCHAR(56) NOT NULL,
       email VARCHAR(50) NOT NULL,
       PRIMARY KEY(email)
       );

CREATE TABLE creneauid(
       idc SERIAL NOT NULL,
       dcreneau time(0) NOT NULL,
       fcreneau time(0) NOT NULL,
       PRIMARY KEY(idc)
       );

CREATE TABLE creneau(
       idc SERIAL NOT NULL,
       idlocal INT NOT NULL,
       idcr INT NOT NULL,
       email VARCHAR(50) NOT NULL,
       jour joursemaine NOT NULL,
       PRIMARY KEY(idc),
       CONSTRAINT salle FOREIGN KEY(idlocal) REFERENCES local(id),
       CONSTRAINT utilisateur FOREIGN KEY(email) REFERENCES utilisateur(email),
       CONSTRAINT creneauid FOREIGN KEY(idcr) REFERENCES creneauid(idc) 
       );

DROP FUNCTION IF EXISTS chevauchement(integer, integer, varchar, varchar);
DROP FUNCTION IF EXISTS chevauchementcreneau(time(0), time(0));

CREATE FUNCTION chevauchement(nnum integer, nidcr integer, nemail varchar, njour varchar)
  RETURNS BOOLEAN AS $$
DECLARE
rec RECORD;

BEGIN
  FOR rec IN 
  SELECT idc, idcr
           FROM creneau
           WHERE (creneau.email = nemail OR creneau.idlocal = nnum)
           AND creneau.jour = cast (njour AS joursemaine)
  LOOP
    IF (rec.idcr = nidcr) THEN
      RETURN TRUE;
    END IF;
  END LOOP;
  RETURN FALSE;
END; $$
LANGUAGE 'plpgsql';


CREATE FUNCTION chevauchementcreneau( dnc time(0), fnc time(0))
  RETURNS BOOLEAN AS $$
DECLARE
rec RECORD;

BEGIN
  FOR rec IN 
  SELECT dcreneau, fcreneau
           FROM creneauid
          
  LOOP
    IF (dnc <= rec.dcreneau AND rec.dcreneau < fnc) OR (rec.dcreneau <= dnc AND dnc < rec.fcreneau) THEN
      RETURN TRUE;
    END IF;
  END LOOP;
  RETURN FALSE;
END; $$
LANGUAGE 'plpgsql';


INSERT INTO utilisateur VALUES('uti1', 'lisateur1', 'azer', 'a@a.com');
INSERT INTO utilisateur VALUES('uti2', 'lisateur2', 'azer', 'b@b.com');
INSERT INTO local(nom) VALUES( 'TP 1');
INSERT INTO local(nom) VALUES('AMPHI 1');
INSERT INTO local(nom) VALUES('TD 1');
INSERT INTO local(nom) VALUES('TP 2');
INSERT INTO local(nom) VALUES( 'AMPHI 2');
INSERT INTO local(nom) VALUES( 'TD 2');
INSERT INTO local(nom) VALUES( 'TP 3');
INSERT INTO local(nom) VALUES( 'AMPHI 3');
INSERT INTO local(nom) VALUES( 'TD 3');
INSERT INTO outil(nom) VALUES('Datashow');
INSERT INTO outil(nom) VALUES('PC');
INSERT INTO outil(nom) VALUES('Places');
insert into creneauid( dcreneau, fcreneau) values( '8:30:00', '10:00:00');
