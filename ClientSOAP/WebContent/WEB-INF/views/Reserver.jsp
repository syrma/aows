<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<%@ page import="com.aows.types.Creneaux"%>
<div class="container">
	<div>
		<h3 class="page-header">Faire une réservation :</h3>
	</div>
	Veuillez choisir un créneau :
	<%Creneaux[] cren = (Creneaux[]) request.getAttribute("creneaux");
	  String disabled = cren != null ? "disabled" : "";%>
	<div class="panel-body">
		<div class="row">
			<form class="panel-body text-center" action="Reserver" method="get"
				accept-charset="utf-8">
				<div class="col-md-4">
				<div class="input-group input-group-top">
						<span class="input-group-addon">Jour</span>
						
						 <select class="form-control" id="joursem" name="joursem">
						
					  <option>Samedi</option>
                      <option>Dimanche</option>
                      <option>Lundi</option>
                      <option>Mardi</option>
                      <option>Mercredi</option>
                      <option>Jeudi</option>
							
						</select>
						
					</div>
					<div class="input-group input-group-top">
						<span class="input-group-addon">créneau</span>
						<%if(cren == null){ 
						out.println("<span class=\"form-control\" > Aucun créneau </span>");
						}else{ %>
						 <select class="form-control" id="jour" name="idcr">
						 <%for(Creneaux c : cren){ %>
							<option value = "<%=c.getIdcr() %>"><%=c %></option>
							<%} %>
						</select>
						<%} %>
					</div>
					<div>
						<button type="submit"
							class="btn btn-success col-md-6 col-md-offset-3 btn-flat input-group">Faire
							une réserveration</button>
					</div>
					</div>
			</form>
		</div>
	</div>
</div>
</body>
</html>