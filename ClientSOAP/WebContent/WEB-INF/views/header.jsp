<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="/TPAOWS/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/TPAOWS/css/style.css">
    <link rel="stylesheet" type="text/css" href="/TPAOWS/css/calendar.css">
    <link rel="stylesheet" type="text/css" href="/TPAOWS/css/test.css">
    <link rel="stylesheet" type="text/css" href="/TPAOWS/css/jquery.datePicker.css">
    <link rel="stylesheet" href="/TPAOWS/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="/TPAOWS/css/bootstrapValidator.min.css">
</head>
<body>
<nav class="navbar ">
    <div class="center-logo text-center">
        <a href="Reservation"> <img src="/TPAOWS/assets/img/logo.png" alt ="..." class="img-circle" height="100"/> </a>

        <h5>Université Abdelhamid Mehri Constantine 2 </h5>
        <h5>Faculté des Nouvelles Technologies de l’Information et de la Communication </h5>


    </div>
    <div class="container-fluid">

        <div class="navbar-header">
            <a class="navbar-brand" href="Reservation">Accueil</a>

        </div>


        <ul class="nav navbar-nav navbar-right">
           

               
                    <li><a href="Deconnexion"><span class="glyphicon glyphicon-off"></span> Deconnexion</a></li>
              

        </ul>
    </div>

</nav> 

