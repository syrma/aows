<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<%@ page import="com.aows.types.Salle"%>
<div class="container">
	<div>
		<h3 class="page-header">Faire une réservation :</h3>
	</div>
	<div class="panel-body">
	Veuillez choisir une salle :
			<form class="panel-body text-center" action="Reserver" method="post"
				accept-charset="utf-8">
       				<input type="hidden" value="<%=request.getAttribute("idcr")%>" name="idcr" /> 
       				<input type="hidden" value="<%=request.getAttribute("joursem")%>" name="joursem" /> 
       				
					<%
						Salle[] s = (Salle[]) request.getAttribute("salles");
					%>
      <div class="col-md-9">
        <div class="panel panel-success panel-successs  ">
          <div class="panel-heading">
          <h3 class="panel-title">Les salles libres</h3>
          </div>
          <div class="panel-body panel-bodyy">   
					<%if(s == null){
						String message = (String) request.getAttribute("erreur");
						out.println("<span class=\"form-control\" >" + message + "</span>");
					}else{
						String[] outils = s[0].nomsOutils();
						%>

                    <table class="table table-hover" id = "salles">
		<div class="row">
                        <thead>
                            <%
                            int col = Math.max(2, 9/outils.length);
                            for(String o : outils){ %>
                        <div class="col-md-<%=col%>">
                            <div class="input-group">
                                <span class="input-group-addon"><%=o %></span>
                                <input type="number" class="form-control" id="<%=o %>" name="<%=o %>" min = "0" value = "0" onchange="filter()">
                            </div>
                        </div>
                        <%} %>
                            <tr>
                              <th style = "text-align: center">Local</th>
                              <% for(String o : outils){ %>
                              <th style = "text-align: center"><%=o %></th>
                              <%} %>
                              <th style = "text-align: center"> Réserver </th>
                            </tr>
                          </thead>
                          <tbody>
                         <% for(Salle salle : s){ %>
                              <tr>
                              <th><%=salle.getNomSalle() %></th>
                              <%for(String o : outils){ %>
                              <td name="outil"><%=salle.nombreOutil(o) %></td>
                              <%} %>
                              <td><input type="radio" name = "salle" value = "<%=salle.getId() %>">
                              </tr>
                             <%}} %>
                          </tbody>
                      </table>
					</div>
                    <div>
                       <button type="submit" class="btn btn-success pull-right input-group">Réserver</button>
                    </div>
				</div>
				</div>
			</form>
		</div>
	</div>
	</div>
</body>
<script>
function filter() {
  // Declare variables
  var input, filter, table, tr, td, i;
  input = document.querySelectorAll('input[type=number]');
  console.log(input[0].value);
//  filter = input.value.toUpperCase();
  table = document.getElementById("salles");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td");
   // alert(td);
    if (td.length > 0) {
    outils : for(j = 0; j < input.length; j++){
      if (+td[j].innerHTML >= +input[j].value) {
        tr[i].style.display = "";
      }else {
        tr[i].style.display = "none";
        break outils;
      }
    }
  }
}
  
}
</script>

</html>