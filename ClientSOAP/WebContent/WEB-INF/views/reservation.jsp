<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<%@ page import="com.aows.types.Creneau"%>
  <div class="container">
          <div>
            <h3 class="page-header">Gestion des réservations :</h3>
          </div>
            <div class="panel-body">
                <div class="row">
                    <form action="Reserver" method="get" accept-charset="utf-8">
                        <div class="col-md-8 ">
                       <button type="submit" class="btn btn-success col-md-6 col-md-offset-3 btn-flat">Faire une réserveration</button></div>
                    </form>
                </div>
            </div>

<% Creneau[] creneau = (Creneau[]) request.getAttribute("reservations"); %> 
      <div class="col-md-9">
        <div class="panel panel-success panel-successs  ">
          <div class="panel-heading">
          <h3 class="panel-title">Mes réservations</h3>
          </div>
          <div class="panel-body panel-bodyy">   
                  
                    <table class="table table-hover" >
                        <thead>
                            <tr>
                              <th>Local</th>
                              <th>Jour</th>
                              <th>Début créneau</th>
                              <th>Fin créneau</th>
                              <th>Annuler</th>
                            </tr>
                          </thead>
                          <tbody>
                             <% if(creneau == null) out.println("<tr><td>"+request.getAttribute("nores")+"</td></tr>");
                             else{ %>
                         <% for(Creneau c : creneau){ %>
                              <tr>
                              <td><%=c.getNomlocal() %></td>
                              <td><%=c.getJour() %></td>
                              <td><%=c.getDcreneau() %></td>
                              <td><%=c.getFcreneau() %></td>
                              <td><form action="Reservation" method = post><input name="salle" type=hidden value ="<%=c.getIdc() %>"> <button type="submit" class="btn btn-danger btn-flat btn-sm"><span class="glyphicon glyphicon-remove"></span> Annuler</button></form></td>
                              </tr>
                             <%}} %>
                          </tbody>
                      </table>
       	   </div>
          <!-- panel-body/ -->    

      </div>
      <!-- success/ -->
      </div>
      <!-- 9/ -->
  </div> 
  <!-- container/ -->

