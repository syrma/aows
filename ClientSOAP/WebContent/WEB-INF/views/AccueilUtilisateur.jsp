<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="/TPAOWS/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/TPAOWS/css/style.css">
    <link rel="stylesheet" type="text/css" href="/TPAOWS/css/calendar.css">
    <link rel="stylesheet" type="text/css" href="/TPAOWS/css/test.css">
    <link rel="stylesheet" type="text/css" href="/TPAOWS/css/jquery.datePicker.css">
    <link rel="stylesheet" href="/TPAOWS/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="/TPAOWS/css/bootstrapValidator.min.css">
</head>
<body>
<%@ page import="com.aows.types.Creneaux"%>
<div class="container">
	<div class=" container" > 
     	        <div class=" text-center col-md-6 col-md-offset-3">
     	        	<img src="/TPAOWS/assets/img/logo.png" alt="..." class="img-circle">
     	        </div>
        	<div class="panel panel-default well col-md-6 col-md-offset-3 form-primary"> 
        	  <div class="panel-heading form-header">
        	  	<h2 class="box-title">Connexion</h2>	
        	  </div>
        		<form class="panel-body text-center" action="Accueil" method="post" accept-charset="utf-8">
        		    <div class="input-group input-group-top">
        		    	<span class="input-group-addon glyphicon glyphicon-user"></span>
        		    	<input class="form-control" type="text" name="email" placeholder="email">
					
        		    </div>
        		    <div class="input-group input-group-top">
        		    	<span class="input-group-addon glyphicon glyphicon-lock"></span>
        		    	<input id="a" class="form-control" type="password" name="mdp" placeholder="mot de passe">
        		    </div>
        			<div >
        				
                <button class="btn btn-success pull-right input-group  " onclick="verifier()" type="submit">   Se connecter  </button>
        			
              </div>

        	   </form>
            
        	</div>
         
        </div>   

</div>
</body>
</html>