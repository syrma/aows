package com.aows.types;

import java.time.LocalTime;

public class Creneau {
	
	@Override
	public String toString() {
		return "Creneau [nomlocal=" + nomlocal + ", dcreneau=" + dcreneau
				+ ", fcreneau=" + fcreneau + ", jour=" + jour + ", email="
				+ email + "]";
	}

	public Creneau() {
		super();
	}

	public String getNomlocal() {
		return nomlocal;
	}
	public void setNomlocal(String nomlocal) {
		this.nomlocal = nomlocal;
	}
	public LocalTime getDcreneau() {
		return dcreneau;
	}
	public void setDcreneau(LocalTime dcreneau) {
		this.dcreneau = dcreneau;
	}
	public LocalTime getFcreneau() {
		return fcreneau;
	}
	public void setFcreneau(LocalTime fcreneau) {
		this.fcreneau = fcreneau;
	}
	public String getJour() {
		return jour;
	}
	public void setJour(String jour) {
		this.jour = jour;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public int getIdc() {
		return idc;
	}

	public void setIdc(int idc) {
		this.idc = idc;
	}

	private String nomlocal;
	private LocalTime dcreneau;
	private LocalTime fcreneau;
	private String jour;
	private String email;
	private int idc;
	private int idlocal;
	public int getIdlocal() {
		return idlocal;
	}

	public void setIdlocal(int idlocal) {
		this.idlocal = idlocal;
	}
	
}

