package com.aows.servlet;

import static com.aows.servlet.SOAPClientSAAJ.createSOAPRequestCreneaux;
import static com.aows.servlet.SOAPClientSAAJ.createSOAPRequestSallesLibres;
import static com.aows.servlet.SOAPClientSAAJ.createSOAPRequestReserver;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPMessage;

import com.aows.types.Creneau;
import com.aows.types.Creneaux;
import com.aows.types.Salle;
/**
 * Servlet implementation class Test
 */
public class Reserver extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String VUE = "/WEB-INF/views/Reserver.jsp";
	private static final String VUE2 = "/WEB-INF/views/Reserver2.jsp";
	private static final String URLLOCAL = "http://localhost:8080/Serveur/services/LocalDAOImpl?wsdl";
	private static final String URLCRENEAU = "http://localhost:8080/Serveur/services/CreneauIdDAOImpl?wsdl";
    private static final String URL = "http://localhost:8080/Serveur/services/CreneauDAOImpl?wsdl";

	/**	
	 * @see HttpServlet#HttpServlet()
	 */
	public void init(){
		
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getSession().getAttribute("email")!=null){ //TODO: remplacer true par le test d'authentification lorsqu'il sera fait
		boolean b=true;
		try{
				int idcr = Integer.parseInt(request.getParameter("idcr"));
				String jour= request.getParameter("joursem");
				SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
				SOAPConnection soapConnection = soapConnectionFactory.createConnection();
				SOAPMessage soapResponse = soapConnection.call(createSOAPRequestSallesLibres(idcr, jour), URLLOCAL);
				System.out.print("Reserver doGet SOAP Message:");
				 soapResponse.writeTo(System.out);
			        System.out.println();
				Salle[] salles = createSOAPRequestSallesLibres(soapResponse);
				if (salles!=null)
				{request.setAttribute("salles", salles);
				request.setAttribute("idcr", idcr);
				request.setAttribute("joursem", jour);
				request.getRequestDispatcher(VUE2).forward(request, response);}
				else{b=false;}
				
			}catch(Exception ex){
				try{
				SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
				SOAPConnection soapConnection = soapConnectionFactory.createConnection();
				SOAPMessage soapResponse = soapConnection.call(createSOAPRequestCreneaux(), URLCRENEAU);
				System.out.print("doGet SOAP Message 2:");
				 soapResponse.writeTo(System.out);
			        System.out.println();
				Creneaux [] c = createSOAPRequestCreneaux(soapResponse);
				request.setAttribute("creneaux", c); 
				request.getRequestDispatcher(VUE).forward(request, response);
				}catch(Exception e){e.printStackTrace();}
			}
			if(b==false){
							request.setAttribute("erreur", "aucune salle libre");
			
		
				request.getRequestDispatcher(VUE2).forward(request, response); 
			}
		}else{
			response.sendRedirect("Accueil");
		}
		/*
		LocalTime dc = LocalTime.of(11, 30);
		LocalTime fc = LocalTime.of(14, 30);
		PrintWriter out = response.getWriter();
		out.println("<html><body>");
//		dao.reserver("a@a.com", 1, "TP", "Mardi", dc, fc);
		dao.annuler(1, "TP", "Mardi", dc);
		out.println("azé");
		out.println("</body></html>");
		 */		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getSession().getAttribute("email")!=null){ //TODO: remplacer true par le test d'authentification lorsqu'il sera fait
			try{
			int salle = Integer.parseInt(request.getParameter("salle"));
			int idcr = Integer.parseInt(request.getParameter("idcr"));
			String jour = request.getParameter("joursem");
			
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();
			  String email=request.getSession().getAttribute("email").toString();
			SOAPMessage soapResponse = soapConnection.call(createSOAPRequestReserver(jour,salle,idcr,email), URL);
				
			//TODO: Capturer la DAOException qui survient si le duo créneau/salle est pris ou si l'enseignant a une autre réservation avec le même créneau
			response.sendRedirect("Reservation");
			}catch(NumberFormatException ex){
				ex.printStackTrace();
				response.sendRedirect("Reservation");
			}catch(Exception ex){

				ex.printStackTrace();
								request.setAttribute("erreur", ex.getMessage());
				request.getRequestDispatcher(VUE2).forward(request, response);
			}
		}else{
			response.sendRedirect("Accueil");
		}
	}

}
