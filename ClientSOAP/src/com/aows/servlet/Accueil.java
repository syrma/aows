package com.aows.servlet;

import static com.aows.servlet.SOAPClientSAAJ.createSOAPRequestAuthentifier;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPMessage;

/**
 * Servlet implementation class Accueil
 */
public class Accueil extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String VUE = "/WEB-INF/views/AccueilUtilisateur.jsp";
	  private static final String URL ="http://localhost:8080/Serveur/services/UtilisateurDAOImpl?wsdl";

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Accueil() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getSession().getAttribute("email")!=null){	response.sendRedirect("Reservation");}
		else request.getRequestDispatcher(VUE).forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getSession().getAttribute("email")!=null){response.sendRedirect("Reservation");} //TODO: remplacer true par le test d'authentification lorsqu'il sera fait
			try{

				String email = request.getParameter("email");
				String mdp = request.getParameter("mdp");
				SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
				SOAPConnection soapConnection = soapConnectionFactory.createConnection();
				// Send SOAP Message to SOAP Server
			
				SOAPMessage soapResponse = soapConnection.call(createSOAPRequestAuthentifier(email,mdp), URL);
			boolean b = createSOAPRequestAuthentifier(soapResponse);
			//	System.out.println("ma réponse " +soapResponse);
				  System.out.println("Response éé Message:");
			        System.out.println(soapResponse);
			        soapResponse.writeTo(System.out);
			        System.out.println();
			
			
			
			if(b){	response.sendRedirect("Reservation");
			request.getSession().setAttribute("email", email);			
			}
			else response.sendRedirect("Accueil");
			}catch(Exception ex){
				response.sendError(500, ex.getMessage());
			}
		
	}

}
