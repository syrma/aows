package com.aows.servlet;

import static com.aows.servlet.SOAPClientSAAJ.createSOAPRequestListe;
import static com.aows.servlet.SOAPClientSAAJ.createSOAPRequestAnnuler;
import static com.aows.servlet.SOAPClientSAAJ.mapListe;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import com.aows.types.Creneau;

/**
 * Servlet implementation class Annuler
 */
public class Reservation extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String VUE = "/WEB-INF/views/reservation.jsp";
    private static final String URL = "http://localhost:8080/Serveur/services/CreneauDAOImpl?wsdl";


	/**
	 * @see HttpServlet#HttpServlet()
	 */

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getSession().getAttribute("email")!=null){ //TODO: remplacer true par le test d'authentification lorsqu'il sera fait
			try{
		        // Create SOAP Connection
		        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
		        SOAPConnection soapConnection = soapConnectionFactory.createConnection();

		        // Send SOAP Message to SOAP Server
		        String url = "http://localhost:8080/Serveur/services/CreneauDAOImpl?wsdl";
		        String email=request.getSession().getAttribute("email").toString();
		        SOAPMessage soapResponse = soapConnection.call(createSOAPRequestListe(email), URL);  //TODO: Remplacer l'e-mail ici par celui de la session
		        Creneau[] c = mapListe(soapResponse);
				request.setAttribute("reservations", c);
			}catch(Exception ex){
				request.setAttribute("nores", "Aucune réservation à afficher");
//				response.getOutputStream().println(ex.getMessage());
			}finally{
			    request.getRequestDispatcher(VUE).forward(request, response);
			}
		}else{
			response.sendRedirect("Accueil");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(true){ //TODO: remplacer true par le test d'authentification lorsqu'il sera fait
			try{
				int salle = Integer.parseInt(request.getParameter("salle"));
				// Create SOAP Connection
				SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
				SOAPConnection soapConnection = soapConnectionFactory.createConnection();

				// Send SOAP Message to SOAP Server
				SOAPMessage soapResponse = soapConnection.call(createSOAPRequestAnnuler(salle), URL);
			}catch(Exception ex){
			}finally{
				response.sendRedirect("Reservation");
			}
		}
	}

}
