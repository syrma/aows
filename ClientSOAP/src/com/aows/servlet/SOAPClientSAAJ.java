package com.aows.servlet;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Iterator;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import com.aows.types.Creneau;
import com.aows.types.Creneaux;
import com.aows.types.Salle;
import com.aows.types.SalleOutil;

public class SOAPClientSAAJ {

	public static void main(String args[]) throws Exception {
		// Create SOAP Connection
		SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
		SOAPConnection soapConnection = soapConnectionFactory.createConnection();

		// Send SOAP Message to SOAP Server
		String url = "http://localhost:8080/Serveur/services/CreneauDAOImpl?wsdl";
		SOAPMessage soapResponse = soapConnection.call(createSOAPRequestAnnuler(1), url);

		// print SOAP Response
		System.out.println("Response SOAP Message:");
		System.out.println(soapResponse);
		soapResponse.writeTo(System.out);
		System.out.println();
		/*        
	        Creneau[] c = mapListe(soapResponse);
        for(int i = 0; i < c.length; i++)
        	System.out.println(c[i]);
		 */      
		soapConnection.close();
	}

	static SOAPMessage createSOAPRequestListe(String mail) throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();

		String serverURI = "http://dao.aows.com";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("example", serverURI);

		/*
        Constructed SOAP Request Message:
        <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:example="http://dao.aows.com">
            <SOAP-ENV:Header/>
            <SOAP-ENV:Body>
                <example:liste>
                    <example:email>a@a.com</example:email>
                </example:liste>
            </SOAP-ENV:Body>
        </SOAP-ENV:Envelope>
		 */

		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement soapBodyElem = soapBody.addChildElement("liste", "example");
		SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("email", "example");
		soapBodyElem1.addTextNode(mail);

		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", serverURI  + "liste");

		soapMessage.saveChanges();

		/* Print the request message */
		System.out.print("Request SOAP Message:");
		soapMessage.writeTo(System.out);
		System.out.println();

		return soapMessage;
	}
	static SOAPMessage createSOAPRequestAuthentifier(String mail, String mdp) throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();

		String serverURI = "http://dao.aows.com";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("example", serverURI);


		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement soapBodyElem = soapBody.addChildElement("authentification", "example");
		SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("email", "example");
		SOAPElement soapBodyElem2 = soapBodyElem.addChildElement("mdp", "example");

		soapBodyElem1.addTextNode(mail);
		soapBodyElem2.addTextNode(mdp);

		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", serverURI  + "authentification");

		soapMessage.saveChanges();

		/* Print the request message */
		System.out.print("Request SOAP Message:");
		soapMessage.writeTo(System.out);
		System.out.println();

		return soapMessage;
	}


	static boolean createSOAPRequestAuthentifier(SOAPMessage soapResponse) throws Exception {
		SOAPBody soapBody = soapResponse.getSOAPBody();
		Iterator itListe = soapBody.getChildElements();
		Iterator itReturn = ((SOAPBodyElement) itListe.next()).getChildElements();
		SOAPBodyElement reservation;
		String b="f";
		while(itReturn.hasNext()){
			reservation = (SOAPBodyElement) itReturn.next();

			String node = reservation.getLocalName();
			if(reservation.getValue()!=null)
				b = reservation.getValue();
		}
		System.out.print("Request SOAP Messageeee:");
		soapResponse.writeTo(System.out);
		System.out.println();
		System.out.println("monb "+b);
		if (b.equals("true")) return true;
		else return false;
	}
	static SOAPMessage createSOAPRequestAnnuler(int idc) throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();

		String serverURI = "http://dao.aows.com";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("example", serverURI);

		/*
        Constructed SOAP Request Message:
        <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:example="http://dao.aows.com">
            <SOAP-ENV:Header/>
            <SOAP-ENV:Body>
                <example:liste>
                    <example:email>a@a.com</example:email>
                </example:liste>
            </SOAP-ENV:Body>
        </SOAP-ENV:Envelope>
		 */

		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement soapBodyElem = soapBody.addChildElement("annuler", "example");
		SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("idc", "example");
		soapBodyElem1.addTextNode(new Integer(idc).toString());

		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", serverURI  + "liste");

		soapMessage.saveChanges();

		/* Print the request message */
		System.out.print("Request SOAP Message:");
		soapMessage.writeTo(System.out);
		System.out.println();

		return soapMessage;
	}

	static Creneau[] mapListe(SOAPMessage soapResponse) throws SOAPException{
		SOAPBody soapBody = soapResponse.getSOAPBody();
		Iterator itListe = soapBody.getChildElements();
		Iterator itReturn = ((SOAPBodyElement) itListe.next()).getChildElements();
		SOAPBodyElement reservation;
		Creneau creneau;
		ArrayList<Creneau> creneaux = new ArrayList<Creneau>();
		while(itReturn.hasNext()){
			reservation = (SOAPBodyElement) itReturn.next();
			Iterator itReservation = reservation.getChildElements();
			creneau = new Creneau();
			while(itReservation.hasNext()){
				SOAPBodyElement reservationElement = (SOAPBodyElement) itReservation.next();
				String node = reservationElement.getLocalName();
				switch(node){
				case "email" : creneau.setEmail(reservationElement.getValue());
				break;
				case "idc" : creneau.setIdc(Integer.parseInt(reservationElement.getValue()));
				break;
				case "idlocal" : creneau.setIdlocal(Integer.parseInt(reservationElement.getValue()));
				break;
				case "jour" : creneau.setJour(reservationElement.getValue());
				break;
				case "nomlocal" : creneau.setNomlocal(reservationElement.getValue());
				break;
				case "dcreneau" :
				case "fcreneau" :
					Iterator<SOAPBodyElement> itcreneau = reservationElement.getChildElements();
					int hour = Integer.parseInt(itcreneau.next().getValue());
					int minute = Integer.parseInt(itcreneau.next().getValue());
					LocalTime value = LocalTime.of(hour, minute);
					if(node.equals("dcreneau"))
						creneau.setDcreneau(value);
					else
						creneau.setFcreneau(value);
				}
			}
			creneaux.add(creneau);
		}
		return creneaux.toArray(new Creneau[0]);

	}

	static SOAPMessage createSOAPRequestSallesLibres(int idcr, String jour) throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();

		String serverURI = "http://dao.aows.com";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("example", serverURI);


		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement soapBodyElem = soapBody.addChildElement("consulterLibres", "example");
		SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("idcr", "example");
		SOAPElement soapBodyElem2 = soapBodyElem.addChildElement("jour", "example");


		soapBodyElem1.addTextNode(new Integer(idcr).toString());
		soapBodyElem2.addTextNode(jour);
		
		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", serverURI  + "consulterLibres");

		soapMessage.saveChanges();

		/* Print the request message */
		System.out.print("Request SOAP Message:");
		soapMessage.writeTo(System.out);
		System.out.println();

		return soapMessage;
	}


	static Salle[] createSOAPRequestSallesLibres(SOAPMessage soapResponse) throws Exception {
		SOAPBody soapBody = soapResponse.getSOAPBody();
		Iterator itListe = soapBody.getChildElements();
		Iterator itReturn = ((SOAPBodyElement) itListe.next()).getChildElements();
		SOAPBodyElement reservation;
		Salle salle;
		ArrayList<Salle> salles = new ArrayList<Salle>();
		ArrayList<SalleOutil> outils;
		while(itReturn.hasNext()){
			reservation = (SOAPBodyElement) itReturn.next();
			Iterator itReservation = reservation.getChildElements();
			salle = new Salle();
			outils = new ArrayList<SalleOutil>();
			while(itReservation.hasNext()){
				SOAPBodyElement reservationElement = (SOAPBodyElement) itReservation.next();
				String node = reservationElement.getLocalName();
				System.out.println(node);
				switch(node){
				case "nomSalle" : salle.setNomSalle(reservationElement.getValue());
				break;
				case "id" : salle.setId(Integer.parseInt(reservationElement.getValue()));
				break;
				case "outils" :
					Iterator<SOAPBodyElement> itoutil = reservationElement.getChildElements();
					SalleOutil outil = new SalleOutil();
					outil.setNom(itoutil.next().getValue());
					outil.setNombre(Integer.parseInt(itoutil.next().getValue()));
					outils.add(outil);
				}
				
			}
			
			salle.setOutils(outils);
			salles.add(salle);
		}
		for(int i=0; i<salles.size();i++)
		{
			System.out.println("ma salle"+salles.get(i).toString());
		}
		return salles.toArray(new Salle[0]);
	}

	static SOAPMessage createSOAPRequestCreneaux() throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();

		String serverURI = "http://dao.aows.com";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("example", serverURI);
		try{

			// SOAP Body
			SOAPBody soapBody = envelope.getBody();
			SOAPElement soapBodyElem = soapBody.addChildElement("creneaux", "example");


			MimeHeaders headers = soapMessage.getMimeHeaders();
			headers.addHeader("SOAPAction", serverURI  + "creneaux");

			soapMessage.saveChanges();
		}catch(Exception e){e.printStackTrace();}
		/* Print the request message */
		System.out.print("Request SOAP Message des creneauuuuuuuux:");
		soapMessage.writeTo(System.out);
		System.out.println();

		return soapMessage;
	}


	static Creneaux[] createSOAPRequestCreneaux(SOAPMessage soapResponse) throws Exception {
		SOAPBody soapBody = soapResponse.getSOAPBody();
		Iterator itListe = soapBody.getChildElements();
		Iterator itReturn = ((SOAPBodyElement) itListe.next()).getChildElements();
		SOAPBodyElement reservation;
		Creneaux creneau;
		ArrayList<Creneaux> creneaux = new ArrayList<Creneaux>();
		while(itReturn.hasNext()){
			reservation = (SOAPBodyElement) itReturn.next();
			Iterator itReservation = reservation.getChildElements();
			creneau = new Creneaux();
			while(itReservation.hasNext()){
				SOAPBodyElement reservationElement = (SOAPBodyElement) itReservation.next();
				String node = reservationElement.getLocalName();
				switch(node){

				case "idcr" : creneau.setIdcr(Integer.parseInt(reservationElement.getValue()));
				break;
				case "dcreneau" :
				case "fcreneau" :
					Iterator<SOAPBodyElement> itcreneau = reservationElement.getChildElements();
					int hour = Integer.parseInt(itcreneau.next().getValue());
					int minute = Integer.parseInt(itcreneau.next().getValue());
					LocalTime value = LocalTime.of(hour, minute);
					if(node.equals("dcreneau"))
						creneau.setDcreneau(value);
					else
						creneau.setFcreneau(value);
				}
			}
			creneaux.add(creneau);
		}
		return creneaux.toArray(new Creneaux[0]);
	}
	static SOAPMessage createSOAPRequestReserver(String jour, int salle, int idcr, String mail) throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();

		String serverURI = "http://dao.aows.com";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("example", serverURI);



		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement soapBodyElem = soapBody.addChildElement("reserver", "example");
		SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("jour", "example");
		SOAPElement soapBodyElem2 = soapBodyElem.addChildElement("salle", "example");
		SOAPElement soapBodyElem3 = soapBodyElem.addChildElement("idcr", "example");
		SOAPElement soapBodyElem4 = soapBodyElem.addChildElement("mail", "example");

		soapBodyElem1.addTextNode(jour);
		soapBodyElem2.addTextNode(new Integer(salle).toString());
		soapBodyElem3.addTextNode(new Integer(idcr).toString());
		soapBodyElem4.addTextNode(mail);

		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", serverURI  + "reserver");

		soapMessage.saveChanges();

		/* Print the request message */
		System.out.print("Request SOAP Message:");
		soapMessage.writeTo(System.out);
		System.out.println();

		return soapMessage;
	}

}
