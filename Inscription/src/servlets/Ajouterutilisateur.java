package servlets;

import static config.Initialisateur.ATTRIBUT_FACTORY;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOFactory;
import dao.UtilisateurDAO;

/**
 * Servlet implementation class Ajouterutilisateur
 */
@WebServlet("/Ajouterutilisateur")
public class Ajouterutilisateur extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String VUE = "/WEB-INF/views/AjouterUtilisateur.jsp";
	private static final String VUE2 = "/WEB-INF/views/Redirection.jsp";

	private UtilisateurDAO udao;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Ajouterutilisateur() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    public void init(){
		DAOFactory df = (DAOFactory) getServletContext().getAttribute(ATTRIBUT_FACTORY);
		udao = df.getUtilisateurDAO();
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher(VUE).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("pnom");
		String email = request.getParameter("email");
		String mdp = request.getParameter("mdp");
		String mdp2=request.getParameter("mdp2");
		if(mdp.equals(mdp2))
		{		System.out.println(udao.ajouter(nom,prenom,email,mdp));
		request.getRequestDispatcher(VUE2).forward(request, response);


		}
		else
		{
			request.getRequestDispatcher(VUE).forward(request, response);
		}
		//udao.ajouter(nom,prenom,email,mdp); //TODO:
	}

}
