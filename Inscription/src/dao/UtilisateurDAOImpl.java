package dao;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


import types.Utilisateur;
public class UtilisateurDAOImpl implements UtilisateurDAO {
	DAOFactory factory;
	private static final String SQL_AJOUTERUTIL = "INSERT INTO utilisateur values (:nom,:prenom,:mdp,:email); ";

	
	
	

	public UtilisateurDAOImpl(DAOFactory daoFactory) {
		factory = daoFactory;
	}

	
	@Override
	public String ajouter(String nom, String prenom, String email, String mdp) {
		Connection cnx = null;
		NamedParameterStatement stmt = null;
		try{
			cnx = factory.getConnection();
			stmt = new NamedParameterStatement(cnx, SQL_AJOUTERUTIL);
			stmt.setString("nom", nom);
			stmt.setString("prenom", prenom);
			stmt.setString("email", email);
			stmt.setString("mdp", mdp);
			System.out.println("SQL à exécuter : \n"+ stmt);
			int i = stmt.executeUpdate();
			
			if(i == 0)
				throw new DAOException("Impossible d'ajouter cet utilisateur.");
			return String.valueOf(i);
		}catch(SQLException ex){
			throw new DAOException(ex);
		}finally{
			
			try{
				stmt.close();
				cnx.close();
			}catch(SQLException e){
				e.printStackTrace();
			}
			
		}
	}

	
	

}
